package myconst;

use strict;
use warnings;
no strict 'refs'; # :( sorry...

=encoding utf8

=head1 NAME

myconst - pragma to create exportable and groupped constants

=head1 VERSION

Version 1.00

=cut

our $VERSION = '1.00';

=head1 SYNOPSIS
package aaa;

use myconst math => {
        PI => 3.14,
        E => 2.7,
    },
    ZERO => 0,
    EMPTY_STRING => '';

package bbb;

use aaa qw/:math PI ZERO/;

print ZERO;             # 0
print PI;               # 3.14
=cut

my $package;
my $callpkg;

my $EXPORT_OK;
my $EXPORT_TAGS;
my $ISA;

sub import {
    $package        = shift;
    $callpkg        = caller;
    $EXPORT_TAGS    = \%{$callpkg."::EXPORT_TAGS"};
    $EXPORT_OK      = \@{$callpkg."::EXPORT_OK"};
    $ISA            = \@{$callpkg."::ISA"};

    if ( scalar(@_) % 2 != 0 ) {
        die "Odd number of args"
    } else {
        my $counter = 0;
        for my $arg ( @_ ) {
            die "Undefined constant name" if ( !$arg and !($counter % 2) );
            $counter++;
        }
    }

    my %consts = @_;
    push @$ISA, "Exporter";

    foreach my $nodename ( keys %consts ) {
        if ( not ref $consts{$nodename} ) {
            access_by_sub($nodename, $consts{$nodename});
        } elsif ( ref $consts{$nodename} eq 'HASH' ) {
            foreach my $internal_nodename ( keys %{$consts{$nodename}} ) {
                die "Incorrect args, cannot create consts" if ref $consts{$nodename}{$internal_nodename};
                access_by_sub($internal_nodename, $consts{$nodename}{$internal_nodename}, $nodename);
            }
        } else {
            die "Incorrect args, cannot create consts";
        }
    }
}

sub access_by_sub {
    my ($key, $value, $tag) = @_;

    die "Invalid constant name" unless $key =~ /^[A-Za-z_][A-Za-z_0-9]*$/;
    die "Invalid constant" unless $value =~ /[A-Za-z0-9_]*/;

    push @$EXPORT_OK, $key;
    push @{$EXPORT_TAGS->{$tag}}, $key if ($tag);
    push @{$EXPORT_TAGS->{all}}, $key;

    $value = quotemeta($value);
    eval("sub $callpkg::$key() { return \"$value\"; }");

    die $@ if $@;
}

1;
